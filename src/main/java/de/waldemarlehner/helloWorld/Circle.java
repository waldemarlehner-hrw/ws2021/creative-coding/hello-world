package de.waldemarlehner.helloWorld;

import de.waldemarlehner.javaListenerUtils.*;
import processing.core.PApplet;

public class Circle implements IDrawable
{
    // Use the factory instead
    private final Func.OneArg<Integer, Integer> sizeFunc;
    private final Func.OneArg<Integer, Integer> colorFunc;
    private final Func.OneArg<Integer, Tuple.Two<Integer, Integer>> positionFunc;
    private final int lifetime;
    private int age = 0;
    private final PApplet canvas;
    private final TopicImpl<Action.NoArgs> deathListener = new TopicImpl<>();

    public Circle( Func.OneArg<Integer, Integer> sizeOverTimeFunc,
                   Func.OneArg<Integer, Integer> colorFunc,
                   Func.OneArg<Integer, Tuple.Two<Integer, Integer>> positionOverTimeFunc,
                   int lifetime,
                   PApplet canvas )
    {
        this.sizeFunc = sizeOverTimeFunc;
        this.colorFunc = colorFunc;
        this.positionFunc = positionOverTimeFunc;
        this.lifetime = lifetime;
        this.canvas = canvas;
    }

    @Override
    public void draw()
    {
        int size = sizeFunc.invoke( age );
        int color = colorFunc.invoke( age );
        Tuple.Two<Integer, Integer> position = positionFunc.invoke( age );

        canvas.fill( color );
        canvas.ellipse( position.A(), position.B(), size, size );
        age++;
        if (age > lifetime)
        {
            this.deathListener.fireListeners( Action.NoArgs::invoke );
        }
    }

    public ITopic<Action.NoArgs> getDeathListener()
    {
        return this.deathListener;
    }

}
