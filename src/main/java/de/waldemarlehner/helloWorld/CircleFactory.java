package de.waldemarlehner.helloWorld;

import de.waldemarlehner.javaListenerUtils.Func;
import de.waldemarlehner.javaListenerUtils.Tuple;
import processing.core.PApplet;

import java.awt.*;
import java.util.Random;

/**
 * Factory used to create a random circle Object based on certain input parameters
 */
public class CircleFactory
{
    private final Random random;
    private final Func.OneArg<Random, Integer> maxSizeGenerator;
    private final Func.OneArg<Random, Integer> lifetimeGenerator;
    private Func.OneArg<Random, Float> hueGenerator;
    private final PApplet canvas;

    public CircleFactory( int seed,
                          int maxSize,
                          int maxSizeDeviation,
                          int lifetime,
                          int lifetimeDeviation,
                          float hue,
                          float hueDeviation,
                          PApplet canvas ) {
        this.random = new Random(seed);
        this.canvas = canvas;

        this.maxSizeGenerator = (Random random) -> {
            float deviationValue = (random.nextFloat() - 0.5f) * 2f;
            return (int)(maxSize + (deviationValue * maxSizeDeviation));
        };
        this.lifetimeGenerator = (Random random) -> {
            float deviationValue = (random.nextFloat() - 0.5f) * 2f;
            return (int)(lifetime + (deviationValue * lifetimeDeviation));
        };
        this.updateHue( hue, hueDeviation );
    }

    public void updateHue(float hue, float hueDeviation) {
        this.hueGenerator = (Random random) -> {
            float deviationValue = (random.nextFloat() - 0.5f) * 2f;
            return (hue + (deviationValue * hueDeviation));
        };
    }

    public Circle createCircleWithPreferredPositionAndDirection(int x, int y, float xEnd, float yEnd) {
        int lifetime = this.lifetimeGenerator.invoke( this.random );
        float hue = this.hueGenerator.invoke( this.random );
        int maxSize = this.maxSizeGenerator.invoke( this.random );


        var colorFunc = this.generateColorFunction( hue, hue+50, lifetime );
        var sizeFunc = this.generateSizeFunc(maxSize, lifetime);
        var positionFunc = this.generatePositionFunction( x, y,xEnd, yEnd, lifetime );
        return new Circle( sizeFunc, colorFunc, positionFunc, lifetime, this.canvas);
    }

    private Func.OneArg<Integer, Tuple.Two<Integer, Integer>> generatePositionFunction( int x, int y, float xEnd, float yEnd, int lifetime) {
        return (Integer currentAge) -> {
            float ageFraction = currentAge.floatValue() / lifetime;
            float currentX = x + ageFraction * (xEnd - (float)x);
            float currentY = y + ageFraction * (yEnd - (float)y);

            return new Tuple.Two<>( (int)currentX, (int)currentY );
        };
    }

    private Func.OneArg<Integer, Integer> generateSizeFunc( int maxSize, int lifetime )
    {
        return (Integer currentAge) -> {
            float ageFraction = currentAge.floatValue() / lifetime;
            float currentSize = ageFraction * maxSize;
            return (int)currentSize;
        };
    }


    private Func.OneArg<Integer, Integer> generateColorFunction(float startingHue, float endingHue, int lifetime) {
        return (Integer currentAge) -> {
            float ageFraction = currentAge.floatValue() / lifetime;
            float currentHue = startingHue + (endingHue - startingHue) * ageFraction;
            Color color = Color.getHSBColor(currentHue / 360, 1, 1);
            return color.getRGB();
        };
    }



}
