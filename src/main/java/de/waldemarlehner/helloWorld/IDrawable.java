package de.waldemarlehner.helloWorld;

public interface IDrawable
{
    void draw();
}
