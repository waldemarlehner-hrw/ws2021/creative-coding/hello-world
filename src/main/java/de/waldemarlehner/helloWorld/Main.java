package de.waldemarlehner.helloWorld;

import de.waldemarlehner.javaListenerUtils.Func;
import de.waldemarlehner.javaListenerUtils.Tuple;
import processing.core.PApplet;

import java.util.LinkedList;
import java.util.List;

public class Main extends PApplet
{
    /// Settings
    private final static float spawnerSpeed = 0.1f;
    private final static Func.OneArg<Integer, Double> radius =
            (frameCounter) -> (10 + 30 * Math.sin( frameCounter * spawnerSpeed * 0 ));
    private final static int screenSize = 500;
    private final static int spawnInterval = 1;
    private final static int distance = 500;
    private final static int circleLifetime = 500;
    ///

    private final List<Circle> circles = new LinkedList<>();
    private final List<Circle> circlesToDelete = new LinkedList<>();
    private CircleFactory circleFactory;
    private int frameCounter = 0;
    private int hue = 0;

    private Tuple.Two<Double, Double> getNewPointPosition()
    {
        int position = frameCounter++;
        double x = Math.sin( -position * spawnerSpeed );
        double y = Math.cos( position * spawnerSpeed );
        x *= radius.invoke( position );
        y *= radius.invoke( position );
        x += ((float) screenSize) / 2;
        y += ((float) screenSize) / 2;

        return new Tuple.Two<>( x, y );
    }


    @Override
    public void settings()
    {
        super.size( screenSize, screenSize );
        this.circleFactory = new CircleFactory( 0, 100, 50, Main.circleLifetime, 10, this.hue, 30, this );

    }

    @Override
    public void setup()
    {
        super.noStroke();
    }

    @Override
    public void draw()
    {

        super.background( 0xB0B0B0 );
        Tuple.Two<Double, Double> newPosition = getNewPointPosition();
        super.fill( 0xAAAAAA );
        super.ellipse( newPosition.A().floatValue(), newPosition.B().floatValue(), 10, 10 );
        if (frameCounter % spawnInterval == 0)
        {
            int startPosX = newPosition.A().intValue();
            int startPosY = newPosition.B().intValue();

            float dirX = newPosition.A().floatValue() - (0.5f * screenSize);
            float dirY = newPosition.B().floatValue() - (0.5f * screenSize);
            double dirLength = Math.sqrt( (dirX * dirX) + (dirY * dirY) );

            dirX = (float) (dirX / dirLength );
            dirY = (float) (dirY / dirLength );

            Circle newCircle = this.circleFactory.createCircleWithPreferredPositionAndDirection(
                    startPosX,
                    startPosY,
                    startPosX + dirX * distance,
                    startPosY + dirY * distance
            );
            this.hue += 1;
            this.circleFactory.updateHue( this.hue, 20 );
            newCircle.getDeathListener().subscribe( () -> this.circlesToDelete.add( newCircle ) );
            this.circles.add( newCircle );
        }

        this.circles.forEach( Circle::draw );
        this.circlesToDelete.forEach( this.circles::remove );
        this.circlesToDelete.clear();
        System.out.println(this.circles.size());
    }

    // Program entry point
    public static void main( String[] args )
    {
        PApplet.main( Main.class, args );
    }

}
